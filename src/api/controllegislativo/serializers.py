from rest_framework import serializers
from .models import (Comision, Diputado, Dictamen, SesionPlenaria, Votacion,
                     PeriodoLegislatura, TipoSesionPlenaria, TipoComision,
                     TipoDictamen, PartidoPolitico, RespuestaVotacion)


class PeriodoLegislaturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeriodoLegislatura
        fields = ('id', 'rango_fecha')


class TipoSesionPlenariaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoSesionPlenaria
        fields = ('id', 'texto_tipo')


class SesionPlenariaSerializer(serializers.ModelSerializer):
    # obtener tipo de sesion plenaria para el frontend
    tipo_sesionplenaria = serializers.CharField(
        source='tipo.texto_tipo', read_only=True)
    # obtener periodo de legislatura de sesion plenaria para el frontend
    periodo_legislatura_rango_fecha = serializers.CharField(
        source='periodo_legislatura.rango_fecha', read_only=True)

    class Meta:
        model = SesionPlenaria
        fields = ('id', 'numero', 'fecha', 'tipo', 'tipo_sesionplenaria',
                  'periodo_legislatura', 'periodo_legislatura_rango_fecha')


class TipoComisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoComision
        fields = ('id', 'texto_tipo')


class ComisionSerializer(serializers.ModelSerializer):
    # obtener tipo de dictamen para el frontend
    tipo_comision = serializers.CharField(
        source='tipo.texto_tipo', read_only=True)

    class Meta:
        model = Comision
        fields = ('id', 'nombre', 'tipo', 'tipo_comision')


class TipoDictamenSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoDictamen
        fields = ('id', 'texto_tipo')


class DictamenSerializer(serializers.ModelSerializer):
    # obtener tipo de dictamen para el frontend
    tipo_dictamen = serializers.CharField(
        source='tipo.texto_tipo', read_only=True)
    # obtener nombre de comision para el frontend
    nombre_comision = serializers.CharField(
        source='id_comision.nombre', read_only=True)
    # obtener numero de sesion plenaria para el frontend
    sesion_plenaria_num = serializers.IntegerField(
        source='id_sesion_plenaria.numero', read_only=True)
    # obtener periodo de legislatura para el frontend
    periodo_legislatura_rango_fecha = serializers.CharField(
        source='periodo_legislatura.rango_fecha', read_only=True)

    class Meta:
        model = Dictamen
        fields = ('id', 'numero', 'tipo', 'tipo_dictamen', 'extracto', 'id_comision', 'nombre_comision', 'documento_link',
                  'id_sesion_plenaria', 'sesion_plenaria_num', 'periodo_legislatura', 'periodo_legislatura_rango_fecha')


class PartidoPoliticoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartidoPolitico
        fields = ('id', 'acronimo', 'nombre')


class DiputadoSerializer(serializers.ModelSerializer):
    # obtener nombre de comision para el frontend
    nombre_comision = serializers.CharField(
        source='id_comision.nombre', read_only=True)
    # # obtener nombre de partido político para el frontend
    nombre_partido = serializers.CharField(
        source='partido.nombre', read_only=True)
    # obtener periodo de legislatura para el frontend
    periodo_legislatura_rango_fecha = serializers.CharField(
        source='periodo_legislatura.rango_fecha', read_only=True)

    class Meta:
        model = Diputado
        fields = ('id', 'nombre', 'id_comision', 'nombre_comision', 'partido',
                  'nombre_partido', 'periodo_legislatura', 'periodo_legislatura_rango_fecha')


class RespuestaVotacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RespuestaVotacion
        fields = ('id', 'texto_respuesta')


class VotacionSerializer(serializers.ModelSerializer):
    # obtener número de dictamen para el frontend
    numero_dictamen = serializers.IntegerField(
        source='id_dictamen.numero', read_only=True)
    # obtener nombre de diputado para el frontend
    nombre_diputado = serializers.CharField(
        source='id_diputado.nombre', read_only=True)
    # obtener respuesta de diputado para el frontend
    respuesta_diputado = serializers.CharField(
        source='respuesta.texto_respuesta', read_only=True)

    class Meta:
        model = Votacion
        fields = ('id', 'id_dictamen', 'numero_dictamen', 'id_diputado',
                  'nombre_diputado', 'respuesta', 'respuesta_diputado')
