from django.shortcuts import render
from rest_framework import generics
from .models import (SesionPlenaria, Comision, Votacion, Dictamen, Diputado,
                     PeriodoLegislatura, TipoSesionPlenaria, TipoComision,
                     TipoDictamen, PartidoPolitico, RespuestaVotacion)
from .serializers import (SesionPlenariaSerializer, ComisionSerializer, VotacionSerializer,
                          DictamenSerializer, DiputadoSerializer, PeriodoLegislaturaSerializer,
                          TipoSesionPlenariaSerializer, TipoComisionSerializer, TipoDictamenSerializer,
                          PartidoPoliticoSerializer, RespuestaVotacionSerializer)

# Create your views here.


class PeriodoLegislaturaView(generics.ListAPIView):
    queryset = PeriodoLegislatura.objects.all()
    serializer_class = PeriodoLegislaturaSerializer


class TipoSesionPlenariaView(generics.ListAPIView):
    queryset = TipoSesionPlenaria.objects.all()
    serializer_class = TipoSesionPlenariaSerializer


class TipoComisionView(generics.ListAPIView):
    queryset = TipoComision.objects.all()
    serializer_class = TipoComisionSerializer


class RespuestaVotacionView(generics.ListAPIView):
    queryset = RespuestaVotacion.objects.all()
    serializer_class = RespuestaVotacionSerializer


class TipoDictamenView(generics.ListAPIView):
    queryset = TipoDictamen.objects.all()
    serializer_class = TipoDictamenSerializer


class PartidoPoliticoView(generics.ListAPIView):
    queryset = PartidoPolitico.objects.all()
    serializer_class = PartidoPoliticoSerializer


# Vistas para listar y crear los datos en la base de datos
class SesionPlenariaListarCrearView(generics.ListCreateAPIView):
    #queryset = SesionPlenaria.objects.all()
    serializer_class = SesionPlenariaSerializer

    # def perform_create(self, serializer):
    #     return serializer.save(sesion = self.request.__str__)

    def get_queryset(self):
        return SesionPlenaria.objects.all()


class ComisionListarCrearView(generics.ListCreateAPIView):
    #queryset = Comision.objects.all()
    serializer_class = ComisionSerializer

    def get_queryset(self):
        return Comision.objects.all()


class VotacionListarCrearView(generics.ListCreateAPIView):
    #queryset = Votacion.objects.all()
    serializer_class = VotacionSerializer

    def get_queryset(self):
        return Votacion.objects.all()


class DictamenListarCrearView(generics.ListCreateAPIView):
    #queryset = Dictamen.objects.all()
    serializer_class = DictamenSerializer

    def get_queryset(self):
        return Dictamen.objects.all()


class DiputadoListarCrearView(generics.ListCreateAPIView):
    #queryset = Diputado.objects.all()
    serializer_class = DiputadoSerializer

    def get_queryset(self):
        return Diputado.objects.all()


# Vistas para actualizar y eliminar los datos en la base de datos
class SesionPlenariaActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = SesionPlenaria.objects.all()
    serializer_class = SesionPlenariaSerializer
    lookup_field = "id"

    # def get_queryset(self):
    #     return self.queryset.filter(periodo_legislatura = 1)


class ComisionActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comision.objects.all()
    serializer_class = ComisionSerializer
    lookup_field = "id"

    # def get_queryset(self):
    #     return self.queryset.filter(periodo_legislatura = 1)


class VotacionActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Votacion.objects.all()
    serializer_class = VotacionSerializer
    lookup_field = "id"


class DictamenActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Dictamen.objects.all()
    serializer_class = DictamenSerializer
    lookup_field = "id"


class DiputadoActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Diputado.objects.all()
    serializer_class = DiputadoSerializer
    lookup_field = "id"
