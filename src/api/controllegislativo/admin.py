from django.contrib import admin
from controllegislativo.models import (SesionPlenaria, Comision, Dictamen, Diputado, Votacion,
                                       PeriodoLegislatura, TipoSesionPlenaria, TipoComision, TipoDictamen,
                                       PartidoPolitico, RespuestaVotacion)

# Register your models here.


class ComisionAdmin(admin.ModelAdmin):
    list_display = ("nombre", "tipo", "id")


class SesionPlenariaAdmin(admin.ModelAdmin):
    list_display = ("numero", "fecha", "tipo", "periodo_legislatura")
    list_filter = ("fecha", "periodo_legislatura")


class DiputadoAdmin(admin.ModelAdmin):
    list_display = ("nombre", "id_comision", "partido", "periodo_legislatura")
    list_filter = ("partido", "id_comision", "periodo_legislatura")


class DictamenAdmin(admin.ModelAdmin):
    list_display = ("numero", "tipo", "id_comision", "documento_link",
                    "id_sesion_plenaria", "periodo_legislatura")
    list_filter = ("id_comision", "tipo", "periodo_legislatura")


class VotacionAdmin(admin.ModelAdmin):
    list_display = ("id_dictamen", "id_diputado", "respuesta")
    list_filter = ("respuesta", "id_diputado")


class PeriodoLegislaturaAdmin(admin.ModelAdmin):
    list_display = ("rango_fecha", "id")


class TipoSesionPlenariaAdmin(admin.ModelAdmin):
    list_display = ("texto_tipo", "id")


class TipoComisionAdmin(admin.ModelAdmin):
    list_display = ("texto_tipo", "id")


class TipoDictamenAdmin(admin.ModelAdmin):
    list_display = ("texto_tipo", "id")


class PartidoPoliticoAdmin(admin.ModelAdmin):
    list_display = ("acronimo", "nombre")


class RespuestaVotacionAdmin(admin.ModelAdmin):
    list_display = ("texto_respuesta", "id")


admin.site.register(SesionPlenaria, SesionPlenariaAdmin)
admin.site.register(Comision, ComisionAdmin)
admin.site.register(Dictamen, DictamenAdmin)
admin.site.register(Diputado, DiputadoAdmin)
admin.site.register(Votacion, VotacionAdmin)
admin.site.register(PeriodoLegislatura, PeriodoLegislaturaAdmin)
admin.site.register(TipoSesionPlenaria, TipoSesionPlenariaAdmin)
admin.site.register(TipoComision, TipoComisionAdmin)
admin.site.register(TipoDictamen, TipoDictamenAdmin)
admin.site.register(PartidoPolitico, PartidoPoliticoAdmin)
admin.site.register(RespuestaVotacion, RespuestaVotacionAdmin)
