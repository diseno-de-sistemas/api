from django.apps import AppConfig


class ControllegislativoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'controllegislativo'
