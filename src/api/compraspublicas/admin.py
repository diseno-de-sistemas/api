from django.contrib import admin
from .models import Institucion, UACI, Proceso, Licitacion, Empresa

# Register your models here.

class InstitucionAdmin(admin.ModelAdmin):
    list_display=("nombre", "direccion", "telefono")

class UACIAdmin(admin.ModelAdmin):
    list_display=("nombre", "direccion", "telefono", "horario_atencion", "nombre_contacto", "telefono_contacto", "fuente_financiamiento")

class ProcesoAdmin(admin.ModelAdmin):
    list_display=("nombre", "costo_base", "estado", "tipo_servicio", "informacion_adicional", "indicaciones_generales", "objeto", "id_institucion", "id_uaci")

class EmpresaAdmin(admin.ModelAdmin):
    list_display=("fecha_adquisicion", "razon_social","telefono", "telefono_fax", "correo_electronico", "nit", "direccion", "especializacion",
    "informacion_adicional")

class LicitacionAdmin(admin.ModelAdmin):
    list_display=("codigo", "nombre", "fecha_publicacion", "tipo", "monto_otorgado", "detalle_anios", "duracion",
    "numero_empleados", "informacion_adicional", "id_empresa")


admin.site.register(Institucion, InstitucionAdmin)
admin.site.register(UACI, UACIAdmin)
admin.site.register(Proceso, ProcesoAdmin)
admin.site.register(Empresa, EmpresaAdmin)
admin.site.register(Licitacion, LicitacionAdmin)