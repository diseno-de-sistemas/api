from django.apps import AppConfig


class CompraspublicasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'compraspublicas'
