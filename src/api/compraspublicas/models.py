from django.db import models

#diccionario de opcciones
servicio=(
    ("Estado", "Estado"),
    ("Obra", "Obra"),
    ("Servicio", "Servicio")
)

# Create your models here.

class Institucion(models.Model):
    #id_institucion=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=50)
    direccion=models.CharField(max_length=100)
    telefono=models.CharField(max_length=25)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural='Instituciones'

class UACI(models.Model):
    #id_uaci=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=50)
    direccion=models.CharField(max_length=100)
    telefono=models.CharField(max_length=25)
    horario_atencion=models.CharField(max_length=30)
    nombre_contacto=models.CharField(max_length=100)
    telefono_contacto=models.CharField(max_length=25)
    fuente_financiamiento=models.CharField(max_length=50)

    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural='UACI'

class Proceso(models.Model):
    #id_Proceso=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=100)
    costo_base=models.DecimalField(max_digits=15, decimal_places=2)
    estado=models.CharField(max_length=50)
    tipo_servicio=models.CharField(choices=servicio, default='Estado', max_length=15)
    informacion_adicional=models.TextField(max_length=10000)
    indicaciones_generales=models.TextField(max_length=10000)
    objeto=models.CharField(max_length=10000)
    id_institucion= models.ForeignKey(Institucion, on_delete=models.CASCADE)
    id_uaci=models.ForeignKey(UACI, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural='Procesos'

class Empresa(models.Model):
    #id_empresa = models.AutoField(db_column='PK_ID_PERSONA', primary_key=True)
    fecha_adquisicion= models.DateField(db_column='FECHA_ADQUISICION_LICITACION')
    razon_social = models.CharField(db_column='RAZON_SOCIAL', max_length=100)
    telefono = models.CharField(db_column='TELEFONO', max_length=10, blank=True, null=True)
    telefono_fax = models.CharField(db_column='TEL_FAX', max_length=10, blank=True, null=True)
    correo_electronico = models.CharField(db_column='CORREO_ELECTRONICO', max_length=30, blank=True, null=True)
    nit = models.CharField(db_column='NIT', max_length=18, blank=True, null=True)
    direccion = models.CharField(db_column='DIRECCION', max_length=60, blank=True, null=True)
    especializacion = models.CharField(db_column='ESPECIALIZACION', max_length=50)
    informacion_adicional = models.TextField(db_column='INFO_ADICIONAL_EMPRESA', max_length=200, blank=True, null=True)

    def __str__(self):
        return self.razon_social
    
    class Meta:
        verbose_name_plural='Empresas'
    
class Licitacion(models.Model):
    #id_licitacion = models.AutoField(db_column='PK_ID_LICITACION', primary_key=True)
    codigo = models.CharField(db_column='CODIGO_LICITACION', max_length=50, blank=True, null=True)
    nombre = models.CharField(db_column='NOMBRE_LICITACION', max_length=100, blank=True, null=True)
    fecha_publicacion = models.DateField(db_column='FECHA_PUBLICACION', blank=True, null=True)
    tipo = models.CharField(db_column='TIPO', max_length=50)
    monto_otorgado = models.DecimalField(db_column='MONTO_OTORGADO', max_digits=15, decimal_places=2, blank=True, null=True)
    detalle_anios = models.IntegerField(db_column='DETALLE_ANIOS', blank=True, null=True)
    duracion = models.IntegerField(db_column='DURACION', blank=True, null=True)
    numero_empleados = models.IntegerField(db_column='NUMERO_EMPLEADOS', blank=True, null=True)
    informacion_adicional = models.TextField(db_column='INFO_ADICIONAL_LICITACION', max_length=200, blank=True, null=True)
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, db_column='FK_ID_EMPRESA', blank=True, null=True)

    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural='Licitaciones'