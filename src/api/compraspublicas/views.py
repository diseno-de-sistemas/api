from django.shortcuts import render
from rest_framework import generics
from .models import Institucion, UACI, Proceso, Empresa, Licitacion
from .serializers import ProcesoSerializer, InstitucionSerializer, UaciSerializer, EmpresaSerializer, LicitacionSerializer

# Create your views here.

#Vistas para listar y crear los datos en la base de datos
class ProcesoListarCrearView(generics.ListCreateAPIView):
    #queryset = Proceso.objects.all()
    serializer_class = ProcesoSerializer

    def get_queryset(self):
        return Proceso.objects.all()

class InstitucionListarCrearView(generics.ListCreateAPIView):
    #queryset = Institucion.objects.all()
    serializer_class = InstitucionSerializer

    def get_queryset(self):
        return Institucion.objects.all()

class UaciListarCrearView(generics.ListCreateAPIView):
    #queryset = UACI.objects.all()
    serializer_class = UaciSerializer

    def get_queryset(self):
        return UACI.objects.all()

class EmpresaListarCrearView(generics.ListCreateAPIView):
    #queryset = Empresa.objects.all()
    serializer_class = EmpresaSerializer

    def get_queryset(self):
        return Empresa.objects.all()

class LicitacionListarCrearView(generics.ListCreateAPIView):
    #queryset = Licitacion.objects.all()
    serializer_class = LicitacionSerializer

    def get_queryset(self):
        return Licitacion.objects.all()



#Vistas para actualizar y eliminar los datos en la base de datos
class ProcesoActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Proceso.objects.all()
    serializer_class = ProcesoSerializer
    lookup_field = "id"

class InstitucionActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Institucion.objects.all()
    serializer_class = InstitucionSerializer
    lookup_field = "id"

class UaciActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = UACI.objects.all()
    serializer_class = UaciSerializer
    lookup_field = "id"

class EmpresaActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Empresa.objects.all()
    serializer_class = EmpresaSerializer
    lookup_field = "id"

class LicitacionActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Licitacion.objects.all()
    serializer_class = LicitacionSerializer
    lookup_field = "id"