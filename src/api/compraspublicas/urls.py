from django.urls import path
from .views import (ProcesoListarCrearView, InstitucionListarCrearView, UaciListarCrearView, EmpresaListarCrearView, LicitacionListarCrearView,
                    ProcesoActualizarEliminarView, InstitucionActualizarEliminarView, UaciActualizarEliminarView, EmpresaActualizarEliminarView, LicitacionActualizarEliminarView)

urlpatterns = [

    # url para ver y crear registros
    path('procesos/', ProcesoListarCrearView.as_view()),
    path('instituciones/', InstitucionListarCrearView.as_view()),
    path('uaci/', UaciListarCrearView.as_view()),
    path('empresa/', EmpresaListarCrearView.as_view()),
    path('licitacion/', LicitacionListarCrearView.as_view()),

    # urls para actualizar y eliminar registros
    path('procesos/modificar/<int:id>/',
         ProcesoActualizarEliminarView.as_view()),
    path('instituciones/modificar/<int:id>/',
         InstitucionActualizarEliminarView.as_view()),
    path('uaci/modificar/<int:id>/', UaciActualizarEliminarView.as_view()),
    path('empresa/modificar/<int:id>/', EmpresaActualizarEliminarView.as_view()),
    path('licitacion/modificar/<int:id>/',
         LicitacionActualizarEliminarView.as_view()),
]
