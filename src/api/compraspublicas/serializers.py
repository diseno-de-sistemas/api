from rest_framework import serializers
from .models import Institucion, UACI, Proceso, Empresa, Licitacion

class ProcesoSerializer(serializers.ModelSerializer):
    #Obtener nombre de la institucion para el frontend
    nombre_institucion = serializers.CharField(source='id_institucion.nombre',read_only=True)
    #Obtener nombre de la uaci para el frontend
    nombre_uaci = serializers.CharField(source='id_uaci.nombre', read_only=True)
    class Meta:
        model = Proceso
        fields = ('id', 'nombre', 'costo_base', 'estado', 'tipo_servicio', 'informacion_adicional', 'indicaciones_generales', 'objeto', 'id_institucion','nombre_institucion', 'id_uaci','nombre_uaci')

class InstitucionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institucion
        fields = ('id','nombre', 'direccion', 'telefono')

class UaciSerializer(serializers.ModelSerializer):
    class Meta:
        model = UACI
        fields = ('id','nombre', 'direccion', 'telefono', 'horario_atencion', 'nombre_contacto', 'telefono_contacto', 'fuente_financiamiento')

class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ('id', 'fecha_adquisicion', 'razon_social', 'telefono', 'telefono_fax', 'correo_electronico', 'nit', 'direccion', 'especializacion', 'informacion_adicional')

        
class LicitacionSerializer(serializers.ModelSerializer):
    #Obtener razon social de la empresa para el front end
    razon_social = serializers.CharField(source='id_empresa.razon_social', read_only=True)
    class Meta:
        model = Licitacion
        fields = ('id', 'codigo', 'nombre','fecha_publicacion', 'tipo', 'monto_otorgado', 'detalle_anios', 'duracion', 'numero_empleados', 'informacion_adicional', 'id_empresa','razon_social')